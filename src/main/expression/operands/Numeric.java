package main.expression.operands;

import main.expression.Helper;
import main.expression.RunnerCalc;
import main.expression.RunnerCalc.Type;

public class Numeric extends RunnerCalc {

	public enum Status{
		NUMBER,
		VARIABLE,
		ERROR
	}

	private double num;

	public Numeric() {

	}

	public Numeric(double n) {
		num = n;
	}

	public static Status checkType(String memb) {
		if(memb == null || memb == "") {
			return Status.ERROR; 
		}else {
			int i = 0;
			boolean isFirstSymbolNum = Helper.isNumeric(memb.charAt(i++));
			if(!isFirstSymbolNum) {
				return Status.VARIABLE;
			}
			boolean isNonSymbolsAlph = true;
			for(;i<memb.length();i++) {
				char sym = memb.charAt(i);
				if(Helper.isNumeric(String.valueOf(sym)) || 
						sym == '.' || sym == ',') {
					continue;
				}else {
					isNonSymbolsAlph = false;
				}
			}
			if(isNonSymbolsAlph) {
				return Status.NUMBER;
			}else {
				return Status.ERROR;
			}
		}
	}

	public static Numeric tryParse(String memb) {
		try {  
			memb = memb.replace(',', '.');
			return new Numeric(Double.parseDouble(memb));  
		} catch(NumberFormatException e){  
			return null;  
		}  
	}

	public Type getType() {
		return Type.OPERAND;
	}

	@Override
	public String toString() {
		return String.valueOf(getNum());
	}

	public double getNum() {
		return num;
	}

}
