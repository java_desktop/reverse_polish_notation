package main.expression.operands;

public class Member {
	
	private String operand;
	
	public Member() {
		
	}
	
	public Member(String op) {
		operand = op;
	}

	public String getMember() {
		return operand;
	}
	
	@Override
	public String toString() {
		return getMember();
	}
	

}
