package main.expression.functions;

public class UnaryFunctionSqrt extends UnaryFunction{

	@Override
	public String getFunction() {
		return "sqrt";
	}

	@Override
	public int getPriority() {
		return 4;
	}

	@Override
	public String run(String op1) {
		try {  
			Double _op1 = Double.parseDouble(op1);
			return String.valueOf(Math.sqrt(_op1));
		} catch(NumberFormatException e){  
			return null;  
		}  
	}

	@Override
	public Type getType() {
		return Type.FUNCTION;
	}

	@Override
	public String toString() {
		return getFunction();
	}
}

