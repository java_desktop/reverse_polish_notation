package main.expression.functions;

import main.expression.Priority;

public abstract class UnaryFunction extends Priority {

	public abstract String getFunction();
	
	public static UnaryFunction tryParse(String func) {
		UnaryFunctionAbs ufAbs = new UnaryFunctionAbs();
		UnaryFunctionCos ufCos = new UnaryFunctionCos();
		UnaryFunctionSin ufSin = new UnaryFunctionSin();
		UnaryFunctionSqrt ufSqrt = new UnaryFunctionSqrt();
		UnaryFunctionTg ufTg = new UnaryFunctionTg();
		
		if(func.equals(ufAbs.getFunction())) {
			return ufAbs;
		}else if(func.equals(ufCos.getFunction())) {
			return ufCos;
		}else if(func.equals(ufSin.getFunction())) {
			return ufSin;
		}else if(func.equals(ufSqrt.getFunction())) {
			return ufSqrt;
		}else if(func.equals(ufTg.getFunction())) {
			return ufTg;
		}
		return null;
	}
	
}
