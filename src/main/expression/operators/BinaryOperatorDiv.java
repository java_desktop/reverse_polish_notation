package main.expression.operators;

public class BinaryOperatorDiv extends BinaryOperator {

	@Override
	public String getOperator() {
		return "/";
	}

	@Override
	public int getPriority() {
		return 2;
	}

	@Override
	public String run(String op1, String op2) {
		try {  
			Double _op1 = Double.parseDouble(op1); 
			Double _op2 = Double.parseDouble(op2); 
			Double val = _op1 / _op2;
			if (val == Double.POSITIVE_INFINITY || val == Double.NEGATIVE_INFINITY) {
				throw new ArithmeticException();
			}
			return String.valueOf(val); 
		} catch(NumberFormatException e){  
			return null;  
		} catch(ArithmeticException e){   
			System.out.println("Error: division by zero");
			return null;  
		} 
	}

	@Override
	public Type getType() {
		return Type.OPERATOR;
	}

	@Override
	public String toString() {
		return getOperator();
	}
}
