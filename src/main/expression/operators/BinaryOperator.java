package main.expression.operators;

import main.expression.Priority;

public abstract class BinaryOperator extends Priority {

	public abstract String getOperator();
	
	public static BinaryOperator tryParse(String operator) {
		BinaryOperatorDiv boDiv = new BinaryOperatorDiv();
		BinaryOperatorLeftBracket boLB = new BinaryOperatorLeftBracket();
		BinaryOperatorRightBracket boRB = new BinaryOperatorRightBracket();
		BinaryOperatorMinus boMin = new BinaryOperatorMinus();
		BinaryOperatorMul boMul = new BinaryOperatorMul();
		BinaryOperatorPlus boPlus = new BinaryOperatorPlus();
		BinaryOperatorPow boPow = new BinaryOperatorPow();
		
		if(operator.equals(boDiv.getOperator())) {
			return boDiv;
		}else if(operator.equals(boLB.getOperator())) {
			return boLB;
		}else if(operator.equals(boRB.getOperator())) {
			return boRB;
		}else if(operator.equals(boMin.getOperator())) {
			return boMin;
		}else if(operator.equals(boMul.getOperator())) {
			return boMul;
		}else if(operator.equals(boPlus.getOperator())) {
			return boPlus;
		}else if(operator.equals(boPow.getOperator())) {
			return boPow;
		}
		return null;
	}
	
}
