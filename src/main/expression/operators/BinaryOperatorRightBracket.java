package main.expression.operators;

public class BinaryOperatorRightBracket extends BinaryOperator {

	@Override
	public String getOperator() {
		return ")";
	}

	@Override
	public int getPriority() {
		return 0;
	}
}

