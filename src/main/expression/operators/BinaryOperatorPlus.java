package main.expression.operators;

public class BinaryOperatorPlus extends BinaryOperator {

	@Override
	public String getOperator() {
		return "+";
	}

	@Override
	public String run(String op1, String op2) {
		try {  
			Double _op1 = Double.parseDouble(op1); 
			Double _op2 = Double.parseDouble(op2); 
			return String.valueOf(_op1 + _op2);
		} catch(NumberFormatException e){  
			return null;  
		}  
	}

	@Override
	public int getPriority() {
		return 1;
	}

	@Override
	public Type getType() {
		return Type.OPERATOR;
	}

	@Override
	public String toString() {
		return getOperator();
	}
}
