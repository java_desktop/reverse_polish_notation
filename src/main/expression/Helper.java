package main.expression;

public class Helper {

	public static boolean isNumeric(String str) {
		try {  
			Double.parseDouble(str);  
			return true;
		} catch(NumberFormatException e){  
			return false;  
		}  
	}

	public static boolean isNumeric(char symbol) {
		try {  
			Double.parseDouble(String.valueOf(symbol));  
			return true;
		} catch(NumberFormatException e){  
			return false;  
		}  
	}


}
