package main.expression;

public abstract class RunnerCalc {

	public enum Type{
		NONE,
		OPERAND,
		OPERATOR,
		FUNCTION
	}

	public String run(String op1) {
		return null;
	}

	public String run(String op1, String op2) {
		return null;
	}

	public Type getType() {
		return Type.NONE;
	}

	@Override
	public String toString() {
		return null;
	}

}
