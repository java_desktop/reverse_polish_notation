package main.expression.pipeline;

import java.util.ArrayList;
import java.util.Stack;

import main.expression.RunnerCalc;
import main.expression.operands.Numeric;

public class CalculatorExpression {

	private Double result;

	public int tryParse(ArrayList<RunnerCalc> expression) {

		Stack<RunnerCalc> calculatorBuf = new Stack<>();
		for(int i = 0; i < expression.size(); i++) {
			RunnerCalc member = expression.get(i);
			if(member.getType() == RunnerCalc.Type.OPERAND) {
				calculatorBuf.push(member);
			} else if(member.getType() == RunnerCalc.Type.FUNCTION) {
				if(!calculatorBuf.empty()) {
					RunnerCalc operand = calculatorBuf.pop();
					String val = member.run(operand.toString());
					if(val == null) {
						return i;
					}
					Numeric newValue = Numeric.tryParse(val);
					if(newValue == null) {
						return i;
					}
					calculatorBuf.push(newValue);
				}else {
					return i;
				}
			} else if(member.getType() == RunnerCalc.Type.OPERATOR) {
				if(calculatorBuf.size() >= 2) {
					RunnerCalc operand1 = calculatorBuf.pop();
					RunnerCalc operand2 = calculatorBuf.pop();
					String val = member.run(operand2.toString(), operand1.toString());
					if(val == null) {
						return i;
					}
					Numeric newValue = Numeric.tryParse(val);
					if(newValue == null) {
						return i;
					}
					calculatorBuf.push(newValue);
				}else {
					return i;
				}
			}
		}
		if(calculatorBuf.size() == 1) {
			Numeric res = (Numeric)calculatorBuf.pop();
			if(res != null) {
				result = res.getNum();
				return RunnerPipeline.NO_ERROR;
			}
		}
		return expression.size();

	}

	public Double getResult(){
		return result;
	}

}

