package main.expression.pipeline;

import java.util.ArrayList;

import main.expression.RunnerCalc;
import main.expression.operands.Member;

public class RunnerPipeline {
	
	public static final int NO_ERROR = -1;

	public void run(String expression) {
		
		// first validation
		if(!expression.matches("[\\w\\d()\\-*\\/-^+.,]*")) {
			System.out.println("Error validation start expression");
			return;
		}
		
		// parse
		ParserExpression parsData = new ParserExpression();
		int index = parsData.tryParse(expression);
		if(index != NO_ERROR) {
			System.out.println("Error parsed expression. Line: " + index);
			return;
		}
		System.out.print("Back poland record: ");
		ArrayList<Member> newExpr = parsData.getResult();
		for(int i = 0; i < newExpr.size(); i++) {
			System.out.print(newExpr.get(i).toString() + " ");
		}
		System.out.println();
		
		// correct
		FixerExpression fixerExpr = new FixerExpression();
		index = fixerExpr.tryParse(newExpr);
		if(index != NO_ERROR) {
			System.out.println("Error correct new expression. Element: " + index);
			return;
		}
		System.out.print("Back poland with values record: ");
		ArrayList<RunnerCalc> correctExpr = fixerExpr.getResult();
		for(int i = 0; i < correctExpr.size(); i++) {
			System.out.print(correctExpr.get(i).toString() + " ");
		}
		System.out.println();
		
		// calculate
		CalculatorExpression calcExpr = new CalculatorExpression();
		index = calcExpr.tryParse(correctExpr);
		if(index != NO_ERROR) {
			System.out.println("Error calculate new expression. Element: " + index);
			return;
		}
		System.out.println("Back poland with values record calculated: " + calcExpr.getResult());
		
		
	}
	
}
