package main.expression.pipeline;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import main.expression.Helper;
import main.expression.Priority;
import main.expression.functions.*;
import main.expression.operands.Member;
import main.expression.operators.*;


public class ParserExpression {
	
	enum ParserStatus{
		END,
		FINDED,
		NOT_FINDED
	}
	
	class Pair{ // C - style
		public ParserStatus status;
		public int offsetIndex;
		
		Pair(ParserStatus status, int offsetIndex){
			this.status = status;
			this.offsetIndex = offsetIndex;
		}
	}
	
	private ArrayList<Member> result;
	
	private static final char LAST_SYMBOL = '=';

	private HashMap<String, Priority> priority = new HashMap<>();

	public ParserExpression() {		
		UnaryFunctionAbs ufAbs = new UnaryFunctionAbs();
		priority.put(ufAbs.getFunction(), ufAbs);
		UnaryFunctionCos ufCos = new UnaryFunctionCos();
		priority.put(ufCos.getFunction(), ufCos);
		UnaryFunctionSin ufSin = new UnaryFunctionSin();
		priority.put(ufSin.getFunction(), ufSin);
		UnaryFunctionSqrt ufSqrt = new UnaryFunctionSqrt();
		priority.put(ufSqrt.getFunction(), ufSqrt);
		UnaryFunctionTg ufTg = new UnaryFunctionTg();
		priority.put(ufTg.getFunction(), ufTg);
		
		BinaryOperatorDiv boDiv = new BinaryOperatorDiv();
		priority.put(boDiv.getOperator(), boDiv);
		BinaryOperatorLeftBracket boLB = new BinaryOperatorLeftBracket();
		priority.put(boLB.getOperator(), boLB);
		BinaryOperatorRightBracket boRB = new BinaryOperatorRightBracket();
		priority.put(boRB.getOperator(), boRB);
		BinaryOperatorMinus boMin = new BinaryOperatorMinus();
		priority.put(boMin.getOperator(), boMin);
		BinaryOperatorMul boMul = new BinaryOperatorMul();
		priority.put(boMul.getOperator(), boMul);
		BinaryOperatorPlus boPlus = new BinaryOperatorPlus();
		priority.put(boPlus.getOperator(), boPlus);
		BinaryOperatorPow boPow = new BinaryOperatorPow();
		priority.put(boPow.getOperator(), boPow);
	}
	
	public int tryParse(String expression) {
		
		Stack<String> calculatorBuf = new Stack<>();
		ArrayList<String> output = new ArrayList<>();
		
		int i = 0;
		BinaryOperatorLeftBracket boLB = new BinaryOperatorLeftBracket(); // (
		BinaryOperatorRightBracket boRB = new BinaryOperatorRightBracket(); // )
		while(true) {
			i = skipSpaces(expression, i);
			if(i == LAST_SYMBOL) {
				break;
			}
			
			String symbol = String.valueOf(getCurrentSymbol(expression, i));
			
			if(symbol.equals(boLB.getOperator())) {
				calculatorBuf.push(boLB.getOperator());
				i++;
				continue;
			}
			
			if(symbol.equals(boRB.getOperator())) {
				while(true) {
					if(calculatorBuf.empty()) {
						return i;
					}
					if(calculatorBuf.peek().equals(boLB.getOperator())) {
						calculatorBuf.pop();
						break;
					}else {
						output.add(calculatorBuf.pop());
					}
				}
				i++;
				continue;
			}
			
			Pair status = isBinaryOperator(expression, i);
			if(status.status == ParserStatus.FINDED) {
				String operator = expression.substring(i, status.offsetIndex);
				operator = operator.replaceAll("\\s", ""); 
				while(true) {
					if(calculatorBuf.empty()) {
						calculatorBuf.push(operator);
						break;
					}
					String topElem = calculatorBuf.peek();
					Priority elemStack = priority.get(topElem);
					if(elemStack == null) {
						return i; 
					}
					Priority elemNonStack = BinaryOperator.tryParse(operator);
					if(elemNonStack == null) {
						return i; 
					}
					if(elemStack.getPriority() >= elemNonStack.getPriority()) {
						output.add(calculatorBuf.pop());
						if(calculatorBuf.empty()) {
							calculatorBuf.push(operator);
							break;
						}
					} else {
						calculatorBuf.push(operator);
						break;
					}
				}
				i = status.offsetIndex;
				continue;
			}
			
			status = isUnaryFunction(expression, i);
			if(status.status == ParserStatus.FINDED) {
				String func = expression.substring(i, status.offsetIndex);
				func = func.replaceAll("\\s", ""); 
				calculatorBuf.push(func);
				i = status.offsetIndex;
				continue;
			}
			
			String operand = "";
			while(true) {
				char sym = getCurrentSymbol(expression, i);
				if(i < expression.length() && (Character.isAlphabetic(sym)
					|| Helper.isNumeric(String.valueOf(sym)) || 
					sym == '.' || sym == ',')) {
					operand += getCurrentSymbol(expression, i);
					i++;
				}else {
					break;
				}
			}
			output.add(operand);
			if(i == LAST_SYMBOL) {
				break;
			}
			
		}
		while(!calculatorBuf.empty()) {
			output.add(calculatorBuf.pop());
		}
		
		convertToClasses(output);
		return RunnerPipeline.NO_ERROR;
	}

	
	private void convertToClasses(ArrayList<String> output) {
		result = new ArrayList<>();
		for(int i = 0; i < output.size() ; i++) {
			result.add(new Member(output.get(i)));
		}
	}
	
	public ArrayList<Member> getResult(){
		return result;
	}
	
	private int skipSpaces(String expression, int currInd) {
		while(currInd < expression.length() && Character.isSpaceChar(expression.charAt(currInd))) { 
			currInd++;
		}
		if(currInd >= expression.length()) {
			return LAST_SYMBOL;
		}
		return currInd;
	}
	
	private char getCurrentSymbol(String expression, int currInd) {
		if(currInd >= expression.length()) {
			return LAST_SYMBOL;
		}
		return expression.charAt(currInd);
	}
	
	private Pair isBinaryOperator(String expression, int currInd) { 
		if(currInd >= expression.length()) {
			return new Pair(ParserStatus.END, -1);
		}
		ArrayList<BinaryOperator> operators = new ArrayList<>() {{
			add(new BinaryOperatorDiv());
			add(new BinaryOperatorMinus());
			add(new BinaryOperatorMul());
			add(new BinaryOperatorPlus());
			add(new BinaryOperatorPow());
		}};
		String symbol = String.valueOf(getCurrentSymbol(expression, currInd));
		for(int i = 0;i < operators.size(); i++) {
			if(operators.get(i).getOperator().equals(symbol)) {
				return new Pair(ParserStatus.FINDED, currInd + 1);
			}
		}
		return new Pair(ParserStatus.NOT_FINDED, -1);
	}
	
	private Pair isUnaryFunction(String expression, int currInd) { 
		ArrayList<UnaryFunction> unFunc = new ArrayList<>() {{
			add(new UnaryFunctionAbs());
			add(new UnaryFunctionCos());
			add(new UnaryFunctionSin());
			add(new UnaryFunctionSqrt());
			add(new UnaryFunctionTg());
		}};
		String expr = "";
		int oldCurrInd = currInd;
		while(true) {
			if(unFunc.size() == 0) {
				return new Pair(ParserStatus.NOT_FINDED, -1);
			}
			currInd = skipSpaces(expression, currInd);
			if(currInd == LAST_SYMBOL) {
				break;
			}
			expr += getCurrentSymbol(expression, currInd);
			if(currInd == LAST_SYMBOL) {
				break;
			}
			ArrayList<UnaryFunction> removeIndex = new ArrayList<>();
			for(int i = 0; i < unFunc.size(); i++) {
				String exprTmp = unFunc.get(i).getFunction();
				if(exprTmp.equals(expr)) {
					return new Pair(ParserStatus.FINDED, currInd + 1);
				}
				if(!exprTmp.startsWith(expr)) {
					removeIndex.add(unFunc.get(i));
				}
			}
			for(int i = 0; i < removeIndex.size(); i++) {
				unFunc.remove((UnaryFunction)removeIndex.get(i));
			}
			currInd++;
		}
		return new Pair(ParserStatus.NOT_FINDED, -1);
	}


}
