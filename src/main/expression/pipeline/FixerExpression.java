package main.expression.pipeline;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import main.expression.RunnerCalc;
import main.expression.functions.UnaryFunction;
import main.expression.operands.Member;
import main.expression.operands.Numeric;
import main.expression.operators.BinaryOperator;

public class FixerExpression {
	
	private ArrayList<RunnerCalc> runnerCalcQueue = new ArrayList();
	
	public int tryParse(ArrayList<Member> expression) {
		
		HashMap<String, Double> varToNum = new HashMap();
		Scanner scanner = new Scanner(System.in);

		for(int i = 0; i < expression.size(); i++) {
			String memb = expression.get(i).getMember();
			UnaryFunction uf = UnaryFunction.tryParse(memb);
			if(uf != null) {
				runnerCalcQueue.add(uf);
				continue;
			}
			BinaryOperator bf = BinaryOperator.tryParse(memb);
			if(bf != null) {
				runnerCalcQueue.add(bf);
				continue;
			}
			Numeric.Status status = Numeric.checkType(memb);
			if(status == Numeric.Status.ERROR) {
				return i;
			}else if(status == Numeric.Status.NUMBER) {
				Numeric num = Numeric.tryParse(memb);
				if(num == null) {
					return i;
				}
				runnerCalcQueue.add(num);
			}else if(status == Numeric.Status.VARIABLE) {
				Numeric num = null;
				for(int j = 0 ; j < memb.length(); j++) {
					char sym = memb.charAt(j);
					if(sym == '.' || sym == ',') {
						return i;
					}
				}
				Double value = varToNum.get(memb);
				if(value == null) {
					System.out.print("Please, write value variable '" + memb + "': ");
					try {
						value = scanner.nextDouble();
					}catch(Exception ex) {
						return i;
					}
					varToNum.put(memb, value);
				}
				num = new Numeric(value);
				runnerCalcQueue.add(num);
			}
		}
		
		return RunnerPipeline.NO_ERROR;
	}
	
	public ArrayList<RunnerCalc> getResult(){
		return runnerCalcQueue;
	}
	
}
