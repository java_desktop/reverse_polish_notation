package main;

import java.util.Scanner;

import main.expression.pipeline.RunnerPipeline;

// Алгоритму плевать на пробелы, он распарсит такое: sqrt(x )-  1 /2*s  i n( x^2- 2 )
// Алгоритм чувствителен к регистру, как в различных языках программирования. Это вызовет ошибку: sqRt(x)-1/2*sin(x^2-2)
// Поддержка операций: sin, cos, tg, abs, -, *, /, +, ^, (, ), латинский алфавит, цифры, '.', ','. В противном случае ошибка.
// Бинарные функции не поддерживаются. Возможно добавить любые бинарные операторы и любые унарные функции
// Поддержка деления на ноль

public class Main {

	public static void main(String [] args) {
		try {

			// example 1
			/*double a = 11;
			double x = 4;
			double U = 3;
			System.out.println(Math.sqrt(a)-1/2.0*Math.sin(Math.pow(x, 2) - 2)*U+Math.pow(x, 2)); 
			// "sqrt(a)-1/2*sin(x^2-2)*U+x^2"*/

			// example 2
			/*double x = 11;
			double U = 8;
			System.out.println(x+4*6+(U*3-Math.sqrt(33-5))-3); 
			// "x+4*6+(U*3-sqrt(33-5))-3"*/

			// example 3
			//System.out.println(74+Math.cos(82)-85/31.0*11+Math.pow(3, 23 + Math.sqrt(11))-12); 
			// "74+cos(82)-85/31*11+3^(23+sqrt(11))-12"

			// example 4
			//System.out.println(74+Math.cos(82)-85/31.0*11.6+Math.pow(3, 23.5 + Math.sqrt(11))-12); 
			// "74+cos(82)-85/31*11,6+3^(23.5+sqrt(11))-12"

			// example 5
			/*double a = 4;
			System.out.println(74+Math.cos(82)-85/31.0*11+Math.pow(3, 23 + Math.sqrt(11))-a); 
			// "74+cos(82)-85/31*11+3^(23+sqrt(11))-a" */

			// example 6 --> error
			// "74+cos(82)-~85/31*11_+3^(23+sqrt(11))-a" */

			// example 7 --> error
			// "74+cos(82)-85/31*11+3^(23+авsqrt(11))-a" */

			// example 8 --> error
			//System.out.println(74+Math.cos(82)-85/31.0*11.6+Math.pow(3, 23.5 + Math.sqrt(11))-12); 
			// "74+cos(82)-85/31*11,6+3^(23.5+Math.sqrt(11))-12"

			// example 9 --> div by zero
			//System.out.println(74+Math.cos(82)-85/0.0*11+Math.pow(3, 23 + Math.sqrt(11))-12); 
			// "74+cos(82)-85/0.0*11+3^(23+sqrt(11))-12"
			
			// example 10 --> error
			//System.out.println(74+Math.cos(82)-85/31.0*11.6+Math.pow(3, 23.5 + Math.sqrt(11))-12); 
			// "74+cos(82.)-85/31*11,,6+3^(23.5+sqrt(11))-12"

			// example 10 --> error
			//System.out.println(74+Math.cos(82)-85/31.0*11.6+Math.pow(3, 23.5 + Math.sqrt(11))-12); 
			// "74+cos(82.)-85/31*11,6+3^(23.5+sq,rt(11))-12"


			System.out.print("Please, write your expression: ");
			Scanner scanner = new Scanner(System.in);			
			String expression = scanner.nextLine();
			System.out.println("Forward record: " + expression);

			RunnerPipeline runner = new RunnerPipeline();
			runner.run(expression);

		}catch(Exception ex) {
			ex.printStackTrace();
		}

	}

}
